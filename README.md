# CS373: Software Engineering Collatz Repo

* Name: (your Full Name)

* EID: (your EID)

* GitLab ID: (your GitLab ID)

* HackerRank ID: (your HackerRank ID)

* Git SHA: (most recent Git SHA, final change to your repo will be adding this value)

* GitLab Pipelines: (link to your GitLab CI Pipeline)

* Estimated completion time: (estimated time in hours, int or float)

* Actual completion time: (actual time in hours, int or float)

* Comments: (any additional comments you have)
